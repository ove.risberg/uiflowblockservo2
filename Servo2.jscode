// Block __Servo2_Init
var __Servo2_Init_json = {
    "previousStatement": null,
    "nextStatement": null,
    "message0": "%1",
    "args0": [
        {
            "type": "field_label",
            "text": "Servo2 Init"
        }
    ],
    "colour": "#008a00"
};

window['Blockly'].Blocks['__Servo2_Init'] = {
    init: function() {
        this.jsonInit(__Servo2_Init_json);
    }
};

window['Blockly'].Python['__Servo2_Init'] = function(block) {
        return `# init I2C
i2c0 = i2c_bus.easyI2C([21,22], 0x40, freq=400000)

# Power off all servos
i2c0.write_u8(0xFA, 0)
i2c0.write_u8(0xFB, 0)
i2c0.write_u8(0xFC, 0)
i2c0.write_u8(0xFD, 0)

# The 16 LEDn outputs are configured with a totem pole structure.
i2c0.write_u8(0x01, 0x04)

# Normal mode
oldmode = i2c0.read_u8(0x00)
newmode = oldmode & 0xEF
i2c0.write_u8(0x00, newmode)

# Set PWM frequency to 50Hz
oldmode = i2c0.read_u8(0x00)
newmode = ( oldmode & 0x7F ) | 0x10
i2c0.write_u8(0x00, newmode)
i2c0.write_u8(0xFE, 122)  # 50Hz
i2c0.write_u8(0x00, oldmode)
wait_ms(5)
i2c0.write_u8(0x00, oldmode | 0x80) # Restart
` + "\n";
};

// Block __Servo2_OFF
var __Servo2_OFF_json = {
    "previousStatement": null,
    "nextStatement": null,
    "message0": "%1",
    "args0": [
        {
            "type": "field_label",
            "text": "Servo2 OFF"
        }
    ],
    "colour": "#008a00"
};

window['Blockly'].Blocks['__Servo2_OFF'] = {
    init: function() {
        this.jsonInit(__Servo2_OFF_json);
    }
};

window['Blockly'].Python['__Servo2_OFF'] = function(block) {
        return `# Power off all servos
i2c0.write_u8(0xFA, 0)
i2c0.write_u8(0xFB, 0)
i2c0.write_u8(0xFC, 0)
i2c0.write_u8(0xFD, 0)` + "\n";
};

// Block __Servo2_pulse
var __Servo2_pulse_json = {
    "previousStatement": null,
    "nextStatement": null,
    "message0": "%1",
    "args0": [
        {
            "type": "field_label",
            "text": "Servo2"
        }
    ],
    "message1": "%1 %2",
    "args1": [
        {
            "type": "field_label",
            "text": "id"
        },
        {
            "type": "input_value",
            "name": "id"
        }
    ],
    "message2": "%1 %2",
    "args2": [
        {
            "type": "field_label",
            "text": "pulse"
        },
        {
            "type": "input_value",
            "name": "pulse"
        }
    ],
    "message3": "%1",
    "args3": [
        {
            "type": "field_label",
            "text": "us"
        }
    ],
    "colour": "#008a00"
};

window['Blockly'].Blocks['__Servo2_pulse'] = {
    init: function() {
        this.jsonInit(__Servo2_pulse_json);
    }
};

window['Blockly'].Python['__Servo2_pulse'] = function(block) {
    var id = Blockly.Python.valueToCode(block, 'id', Blockly.Python.ORDER_NONE);
var pulse = Blockly.Python.valueToCode(block, 'pulse', Blockly.Python.ORDER_NONE);
    return `# pulse: min = 1000 and max = 2000
pulse = ${pulse}
if pulse == 0:
    # pulse = 0 will turn servo OFF
    value = 0
else:
    if pulse < 1000:
        pulse = 1000
    if pulse > 2000:
        pulse = 2000
    # value: min = 105 and max: 524
    value = int(105 + ((524 - 105) / 1000) * (pulse - 1000))
i2c0.write_u8(0x06 + 4 * ${id}, 0)
i2c0.write_u8(0x07 + 4 * ${id}, 0)
i2c0.write_u8(0x08 + 4 * ${id}, value & 0xFF)
i2c0.write_u8(0x09 + 4 * ${id}, (value >> 8) & 0xFF)` + "\n";
};

// Block __Servo2_angle
var __Servo2_angle_json = {
    "previousStatement": null,
    "nextStatement": null,
    "message0": "%1",
    "args0": [
        {
            "type": "field_label",
            "text": "Servo2"
        }
    ],
    "message1": "%1 %2",
    "args1": [
        {
            "type": "field_label",
            "text": "id"
        },
        {
            "type": "input_value",
            "name": "id"
        }
    ],
    "message2": "%1 %2",
    "args2": [
        {
            "type": "field_label",
            "text": "angle"
        },
        {
            "type": "input_value",
            "name": "angle"
        }
    ],
    "message3": "%1",
    "args3": [
        {
            "type": "field_label",
            "text": ""
        }
    ],
    "colour": "#008a00"
};

window['Blockly'].Blocks['__Servo2_angle'] = {
    init: function() {
        this.jsonInit(__Servo2_angle_json);
    }
};

window['Blockly'].Python['__Servo2_angle'] = function(block) {
    var id = Blockly.Python.valueToCode(block, 'id', Blockly.Python.ORDER_NONE);
var angle = Blockly.Python.valueToCode(block, 'angle', Blockly.Python.ORDER_NONE);
    return `# angle: min = 0 and max = 180
angle = ${angle}
if angle < 0:
    angle = 0
if angle > 180:
    angle = 180
# value: min = 105 and max: 524
value = int(105 + ((524 - 105) / 180) * angle)
i2c0.write_u8(0x06 + 4 * ${id}, 0)
i2c0.write_u8(0x07 + 4 * ${id}, 0)
i2c0.write_u8(0x08 + 4 * ${id}, value & 0xFF)
i2c0.write_u8(0x09 + 4 * ${id}, (value >> 8) & 0xFF)` + "\n";
};

// Block __Servo2_external_power
var __Servo2_external_power_json = {
    "previousStatement": null,
    "nextStatement": null,
    "message0": "%1",
    "args0": [
        {
            "type": "field_label",
            "text": "Servo2 enable external power"
        }
    ],
    "colour": "#008a00"
};

window['Blockly'].Blocks['__Servo2_external_power'] = {
    init: function() {
        this.jsonInit(__Servo2_external_power_json);
    }
};

window['Blockly'].Python['__Servo2_external_power'] = function(block) {
        return `# Python code for  M5.Axp.SetBusPowerMode(1);
data = None

i2c1 = i2c_bus.easyI2C((21, 22), 0x34, freq=400000)

data = i2c1.read_u8(0x10)
data = (data & (~ (0x01 << 2)))
i2c1.write_mem_data(0x10, data, i2c_bus.UINT8LE)

data = i2c1.read_u8(0x90)
data = data | 0x07
i2c1.write_mem_data(0x90, data, i2c_bus.UINT8LE)` + "\n";
};

