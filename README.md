# Custom UIFlow Block for Servo2 module

![alt text](Servo2.png)

I could not find any support for Servo2 module in UIFlow so I created a Custom UIFlow module

## Load custom module into UIFlow

Click on "Custom" and then on "Open \*.m5b file" and select Servo2.m5b

New Servo2 blocks will appear under "Custom/Servo2"

Conversion between Servo2.m5b and Servo2.jscode is made using m5b2jscode and jscode2m5b found in https://gitlab.com/ove.risberg/uiflowblocktools

## Usage

The "Servo2 Init" block must be used before any of the other new blocks since it will initialize the i2c communication to the Servo2 module

"Servo2 OFF" will turn off all servos

"Servo2 pulse" will set the pulse length between 1000uS and 2000uS. If pulse length is set to 0 the servo will be urned off.

"Servo2 angle" will set the angle between 0 and 180 degrees.

"Servo2 enable external power" will enable power from the Servo2 module so the internal battery can be charged.
